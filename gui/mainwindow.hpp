//
// Created by Vi1i on 1/24/19.
//

#ifndef AXEL_GUI_MAINWINDOW_HPP
#define AXEL_GUI_MAINWINDOW_HPP

#include <QMainWindow>

class QAction;
class QActionGroup;
class QLabel;
class QMenu;

namespace axel { namespace gui {
    class mainwindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit mainwindow();


    protected:
#ifndef QT_NO_CONTEXTMENU
        void contextMenuEvent(QContextMenuEvent *event) override;
#endif // QT_NO_CONTEXTMENU


    private slots:
        void newFile();
        void open();
        void save();
        void print();
        void undo();
        void redo();
        void cut();
        void copy();
        void paste();
        void bold();
        void italic();
        void leftAlign();
        void rightAlign();
        void justify();
        void center();
        void setLineSpacing();
        void setParagraphSpacing();
        void about();
        void aboutQt();

    private:
        void createActions();
        void createMenus();

        QMenu *fileMenu;
        QMenu *editMenu;
        QMenu *formatMenu;
        QMenu *helpMenu;
        QActionGroup *alignmentGroup;
        QAction *newAct;
        QAction *openAct;
        QAction *saveAct;
        QAction *printAct;
        QAction *exitAct;
        QAction *undoAct;
        QAction *redoAct;
        QAction *cutAct;
        QAction *copyAct;
        QAction *pasteAct;
        QAction *boldAct;
        QAction *italicAct;
        QAction *leftAlignAct;
        QAction *rightAlignAct;
        QAction *justifyAct;
        QAction *centerAct;
        QAction *setLineSpacingAct;
        QAction *setParagraphSpacingAct;
        QAction *aboutAct;
        QAction *aboutQtAct;
        QLabel *infoLabel;
    };
}}
#endif //AXEL_GUI_MAINWINDOW_HPP
